!module containing routines to compute the mean and variance of an array
module stats
    implicit none
    integer :: N !size of array (set in calling program)
    real(kind=8), allocatable, dimension(:) :: f !array set in calling program
    save
contains

!----------------------------
subroutine initialize_stats()
!Read in N from input file, data.in
!Allocate the size of f accordingly
    implicit none

end subroutine initialize_stats

!----------------------
function compute_mean()
    implicit none
!Compute mean using f and N

end function compute_mean

!---------------------------------
subroutine compute_var(fmean,fvar)
!Compute variance using f,N, and input variable fmean
!Return the variance via the output variable, fvar
!Variable declarations for fmean and fvar will need to be added
    implicit none

end subroutine compute_var

!---------------
end module stats


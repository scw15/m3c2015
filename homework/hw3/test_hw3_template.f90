!program to test fdmodule in hw3_dev.f90
program test_fdmodule
    use fdmodule
    !For this code to work, the user must:
    !1. add variable declarations, alpha (double prec),
    !and error (array of double prec.)
    !2. tell the code where it can find information on dx, N, and test_fd
    implicit none
    real(kind=8), dimension(2) :: error
    real(kind=8) :: alpha

    open(unit=10,file='data.in')
        read(10,*) N
        read(10,*) alpha
    close(10)

    dx = 2.d0/dble(N-1)

    call test_fd(alpha,error)

    call show_n()

! or:    call test_fd_time(alpha,error,time)
    print *, 'N=',N
    print *, 'error fd2, cfd4 =',error(:)
!or:    print *, 'time fd2, cfd4 =',time(:)

end program test_fdmodule

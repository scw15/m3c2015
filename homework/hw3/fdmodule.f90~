!Module containing routines for differentiating array of size
!2nd order and 4th order compact finite differences
!Test functions apply these methods to a Gaussian function and
!return performance information.

module fdmodule
    implicit none
    integer :: N
    real(kind=8) :: dx
    save

contains

!-------------------

subroutine show_n()
    print *, N
end subroutine show_n

!-------------------

subroutine fd2(f,df)
    !2nd order centered finite difference
    !Header variables
    !f: function to be differentiated
    !df: derivative of f
    implicit none
    real(kind=8), dimension(:), intent(in) :: f
    real(kind=8), dimension(size(f)), intent(out) :: df
    integer :: i

    df(1) = (f(2)-f(N))/(2*dx)
    df(N) = (f(1)-f(N-1))/(2*dx)

    do i=2,N-1
        df(i) = (f(i+1)-f(i-1))/(2*dx)
    end do
end subroutine fd2

!-----------------

subroutine cfd4(f,df)
    !compact 4th order centered finite difference
    !Header variables
    !f: function to be differentiated
    !df: derivative of f
    implicit none
    real(kind=8), dimension(:), intent(in) :: f
    real(kind=8), dimension(size(f)), intent(out) :: df
    integer :: i, NRHS, LDB, INFO
    real(kind=8), dimension(N-1) :: DL, DU
    real(kind=8), dimension(N) :: D, B, BTemp

    LDB = N
    NRHS = 1

    ! Initialise D and B
    ! D = diagonal elements of A, for A*f' = B
    B(1) = -2.5*f(1) + 2.d0*f(2) + 0.5*f(3)
    B(N) = 2.5*f(N) - 2.d0*f(N-1) - 0.5*f(N-2)
    D(1) = 1
    D(N) = 1
    do i = 2, N-1
        D(i) = 4
        B(i) = 3*(f(i+1) - f(i-1))
    end do

    B = B / dx

    ! Initialise DL and DU, the sub- and super-diagonal components of A
    DL(1) = 1
    DL(N-1) = 2
    DU(1) = 2
    DU(N-1) = 1
    do i = 2, N-2
        DL(i) = 1
        DU(i) = 1
    end do

    ! Initialisation complete - now apply lapack library
    BTemp = B
    call dgtsv(N, NRHS, DL, D, DU, BTemp, LDB, INFO)

    !print *, INFO

    df = BTemp

end subroutine cfd4
!------------------

subroutine test_fd(alpha,error)
    !test accuracy of fd2 and cfd4 with a Gaussian function, f=exp(-alpha*(x-x0)**2)
    !Header variables:
    !alpha: sets width of Gaussian
    !error: 2-element array containing fd2 and cfd4 approximation errors
    implicit none
    integer :: i1
    real(kind=8), intent(in) :: alpha
    real(kind=8), intent(out) :: error(2)
    real(kind=8), dimension(N) :: x, fg, dfg_fd2, dfg_cfd4
    real(kind=8) :: x0, error_fd2, error_cfd4

    do i1 = 1, N
        x(i1) = (i1-1.d0)*dx
    end do

    x0 = x(N)/2.d0

    do i1 = 1, N
        fg(i1) = exp(-alpha * (x(i1)-x0)**2) !Exact value for fg
    end do

    ! Calculate error of differentiation functions for this Gaussian

    error_fd2 = 0
    error_cfd4 = 0

    call fd2(fg, dfg_fd2) ! dfg estimated by fd2
    call cfd4(fg, dfg_cfd4) ! dfg estimated by cfd4

    ! Compare with analytic value, summing errors
    do i1 = 1, N
        error_fd2 = error_fd2 + abs(dfg_fd2(i1) + 2*alpha*(x(i1)-x0)*fg(i1))
        error_cfd4 = error_cfd4 + abs(dfg_cfd4(i1) + 2*alpha*(x(i1)-x0)*fg(i1))
    end do

    error(1) = error_fd2/N
    error(2) = error_cfd4/N

end subroutine test_fd
!---------------------

subroutine test_fd_time(alpha,error,time)
    !test accuracy and speed of fd2 and cfd4 with a Gaussian function, f=exp(-alpha*(x-x0)**2)
    !Header variables:
    !alpha: sets width of Gaussian
    !error: 2-element array containing fd2 and cfd4 approximation errors
    !time: 2-element array containing wall-clock time required by 1000 calls
    !to fd2 and cfd4
    implicit none
    integer :: i1
    real(kind=8), intent(in) :: alpha
    real(kind=8), intent(out) :: error(2),time(2)
    real(kind=8), dimension(N) :: x, fg, dfg_fd2, dfg_cfd4
    real(kind=8) :: x0, error_fd2, error_cfd4
    real(kind=8) :: time_counter1, time_counter2

    do i1 = 1, N
        x(i1) = (i1-1.d0)*dx
    end do

    x0 = x(N)/2.d0

    do i1 = 1, N
        fg(i1) = exp(-alpha * (x(i1)-x0)**2) !Exact value for fg
    end do

    ! Calculate error of differentiation functions for this Gaussian

    error_fd2 = 0
    error_cfd4 = 0

    call system_clock(time_counter1)

    do i1 = 1, 1000
        call fd2(fg, dfg_fd2) ! dfg estimated by fd2
    end do
    
    call system_clock(time_counter2)
    time(1) = time_counter2 - time_counter1
    call system_clock(time_counter1)

    do i1 = 1, 1000
        call cfd4(fg, dfg_cfd4) ! dfg estimated by cfd4
    end do

    call system_clock(time_counter2)
    time(2) = time_counter2 - time_counter1

    ! Compare with analytic value, summing errors
    do i1 = 1, N
        error_fd2 = error_fd2 + abs(dfg_fd2(i1) + 2*alpha*(x(i1)-x0)*fg(i1))
        error_cfd4 = error_cfd4 + abs(dfg_cfd4(i1) + 2*alpha*(x(i1)-x0)*fg(i1))
    end do

    error(1) = error_fd2/N
    error(2) = error_cfd4/N

end subroutine test_fd_time
!---------------------------
end module fdmodule

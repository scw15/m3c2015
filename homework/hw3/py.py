"""Assess accuracy and perfomance of fortran finite difference module, fdmodule.
Converted to python module fdmodule.so using f2py
"""

from fdmodule import fdmodule as f1 #you may change this line if desired
import numpy as np
import matplotlib.pyplot as plt

def fdtest_a(alpha=100.0):
    Nlist = [100*pow(2, i) for i in range(8)]
    n = len(Nlist)
    error = np.zeros([2, n], dtype=np.double)
    for i in range(n):
        f1.n = Nlist[i]
        f1.dx = 2.0/(Nlist[i]-1)
        error[:, i] = f1.test_fd(alpha)
    m = [np.polyfit(np.log(Nlist), np.log(error[i, :]), 1)[0] for i in range(len(error))]
    plt.plot(Nlist, error[0,:], '.')
    plt.plot(Nlist, error[1,:], '.')
    plt.xlabel('N')
    plt.ylabel('Error')
    plt.xscale('log')
    plt.yscale('log')
    plt.legend(['2nd order, m={0:.4f}'.format(abs(m[0])), '4th order, m={0:.4f}'.format(abs(m[1]))])
    plt.xlim([0, 20000])
    plt.show()
    return error, m
    
    
def fdtest_at(alpha=100.0):
    """alpha: input variable which sets width of 
    Guassian function to be differentiated
    """ 
    Nlist = [100*pow(2, i) for i in range(8)]
    n = len(Nlist)
    error = np.zeros([2, n], dtype=np.double)
    walltime = np.zeros([2, n], dtype=np.double)
    for i in range(n):
        f1.n = Nlist[i]
        f1.dx = 2.0/(Nlist[i]-1)
        error[:, i], walltime[:, i] = f1.test_fd_time(alpha)
    m = [np.polyfit(np.log(Nlist), np.log(error[i, :]), 1)[0] for i in range(len(error))]
    plt.plot(Nlist, error[0,:], '.')
    plt.plot(Nlist, error[1,:], '.')
    plt.xlabel('N')
    plt.ylabel('Error')
    plt.xscale('log')
    plt.yscale('log')
    plt.legend(['2nd order, m={0:.4f}'.format(abs(m[0])), '4th order, m={0:.4f}'.format(abs(m[1]))])
    plt.xlim([0, 20000])
    plt.show()
    m_time = [np.polyfit(np.log(Nlist), np.log(walltime[i, :]), 1)[0] for i in range(len(error))]
    plt.plot(Nlist, walltime[0,:], '.')
    plt.plot(Nlist, walltime[1,:], '.')
    plt.xlabel('N')
    plt.ylabel('Walltime')
    plt.xscale('log')
    plt.yscale('log')
    plt.legend(['2nd order, m={0:.4f}'.format(abs(m[0])), '4th order, m={0:.4f}'.format(abs(m[1]))])
    plt.xlim([0, 20000])
    plt.show()
    return error, m, walltime


def fdtest_eff(alpha=100.0):
    Nlist = [100*pow(2, i) for i in range(8)]
    n = len(Nlist)
    error = np.zeros([2, n], dtype=np.double)
    walltime = np.zeros([2, n], dtype=np.double)
    for i in range(n):
        f1.n = Nlist[i]
        f1.dx = 2.0/(Nlist[i]-1)
        error[:, i], walltime[:, i] = f1.test_fd_time(alpha)
    plt.plot(walltime[0,:], error[0,:], '.')
    plt.plot(walltime[1,:], error[1,:], '.')
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel('Walltime')
    plt.ylabel('Error')
    plt.legend(['2nd order', '4th order'])
    #plt.show()
    plt.savefig("hw3.png")
    
#This section will be used for assessment, you may enter a call to fdtest_eff, but
#the three un-commented lines should be left as is in your final submission.    
if __name__ == "__main__":
    e1,m=fdtest_a(alpha=125.0)
    #e2,m,t2 = fdtest_at()
    #add function call to fdtest_eff here if needed
    #fdtest_eff()
    #plt.show()

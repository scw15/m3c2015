"""Solve advection equation with odeint and fortran fd2 routine in fdmodule


"""    
    
import f1 #you may change this line if desired
import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt
from scipy.integrate import odeint



def advection1(tf,n,dx,c=1.0,S=0.0,display=False):
    """solve advection eqn, df/dt + c df/dx = S 
    where c and S are constants, x=0,dx,...(N-1)*dx,
    and f is periodic, f(x_i) = f(x_i+N*dx).
    Initial condition: f(x,t=0) = exp(-100.0*(x-x0)**2)
    Returns: f(x,tf)
    """
    f1.fdmodule.n = n
    f1.fdmodule.dx = dx
    t = np.linspace(0, tf, 50)
    x = np.linspace(0, (n-1)*dx, n)
    x0 = (n-1)*dx/2.0
    f0 = np.exp(-100*pow(x-x0, 2))
    #function provides RHS to odeint
    def RHS(f,t,dx,n,c,S):
        return S - c * f1.fdmodule.fd2(f)
    solution = odeint(RHS, f0, t, (dx, n, c, S), mxstep=50000)
    if display == True:
#        plt.plot(x, solution[0,:])
#        plt.plot(x, solution[50/4.0,:])
#        plt.plot(x, solution[50/2.0,:])
#        plt.plot(x, solution[3.0*50/4.0,:])
        plt.plot(x, solution[-1,:])
        plt.legend(['t = %d' % tf])
#        plt.legend(['t = 0', 't = tf/4', 't = tf/2', 't = 3*tf/4', 't = tf'])
        plt.xlabel('x')
        plt.ylabel('f(x, tf)')
    return solution[-1,:]
    
  
def test_advection1():
    tf = 1
    c = 1
    S = 1
    Nlist = [pow(2, i+5) for i in range(8)]
    errors = []
    for n in Nlist:
        dx = 1.0/n
        simulation = advection1(tf,n,dx,c,S)
        x0 = (n-1)*dx/2.0
        x = np.linspace(0, (n-1)*dx, n)
        # Had difficulty getting analytic solution for general tf...
        #analytic = tf*S + np.exp(-100*pow((x-x0-c*tf)%(n*dx), 2))
        # But this way is fine (knowing that for tf=1, the final
        # analytic solution is just the original condition translated
        # vertically by tf*S
        analytic = tf*S + np.exp(-100.0*pow((x-x0), 2))
        errors.append(sum(abs(analytic - simulation))/n)
    plt.loglog(Nlist, errors, 'x')
    plt.xlabel('N')
    plt.ylabel('Error')
    m = abs(np.polyfit(np.log(Nlist), np.log(errors), 1)[0])
    try:
        assert (m <= 2.1) & (m >= 1.9)
    except AssertionError:
        print "Incorrect convergence rate"
    return m
    
                                                                               
#This section will be used for assessment, you may enter a call to fdtest_eff, but
#the three un-commented lines should be left as is in your final submission.    
if __name__ == "__main__":
    n = 1000
    dx = 1.0/float(n-1)
    #f = advection1(2,n,dx,1,1,True)
    m = test_advection1()
    plt.show()

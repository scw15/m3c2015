!module containing routines for differentiating array of size N with
!2nd order and 4th order compact finite differences
!Test function applies these methods to a Gaussian function and 
!returns the error.

module fdmodule2d
    use omp_lib
    use fdmodule
    implicit none
    integer :: n1,n2,numthreads
    real(kind=8) :: dx1,dx2
    save
contains
!-------------------
subroutine grad(f,df1,df2,df_amp,df_max)
    implicit none
    real(kind=8), dimension(:,:), intent(in) :: f
    real(kind=8), dimension(size(f,1),size(f,2)), intent(out) :: df1,df2,df_amp
    real(kind=8), intent(out) :: df_max
    integer :: i, j

    N = n1
    dx = dx1

    do i = 1, n1
        call cfd4(f(i,:), df2(i,:))
    end do

    N = n2
    dx = dx2

    do j = 1, n2
        call cfd4(f(:,j), df1(:,j))
    end do

    df_amp = abs(df2) + abs(df1)

    df_max = maxval(df_amp)

end subroutine grad
!---------------------------
subroutine test_grad(error,time)
    !tests accuracy and speed of grad, assumes n1,n2,dx1,dx2 have been set in calling program
    implicit none
    integer :: i1,j1
    real(kind=8), allocatable, dimension(:) :: x1,x2 !coordinates stored in arrays
    real(kind=8), allocatable, dimension(:,:) :: x11,x22 !coordinates stored in matrices
    real(kind=8), allocatable, dimension(:,:) :: ftest,df1,df2,df_amp,df_amp_exact !test function and results frorom grad
    real(kind=8) :: df_max
    real(kind=8), intent(out) :: time,error(2) !error is two-element array
    integer :: time_counter1, time_counter2

    allocate(x1(n1),x2(n2),x11(n2,n1),x22(n2,n1),ftest(n2,n1),df1(n2,n1),df2(n2,n1), &
        df_amp(n2,n1),df_amp_exact(n2,n1))

    !generate mesh
    dx1 = 1.d0/dble(n1-1)
    dx2 = 1.d0/dble(n2-1)

    do i1=1,n1
        x1(i1) = dble(i1-1)*dx1
    end do

    do i1=1,n2
        x2(i1) = dble(i1-1)*dx2
    end do

    do i1=1,n2
        x11(i1,:) = x1
    end do

    do i1=1,n1
        x22(:,i1) = x2
    end do

    ftest = sin(x11)*cos(x22) !test function

    ! Perform grad calculation, recording wall time
    call system_clock(time_counter1)
    call grad(ftest,df1,df2,df_amp,df_max)
    call system_clock(time_counter2)
    time = time_counter2 - time_counter1

    ! Calculate error in grad compared to analytical value
    error(1) = sum(abs(cos(x11)*cos(x22) - df1))
    error(2) = sum(abs(-sin(x11)*sin(x22) - df2))

end subroutine test_grad
!-------------------
subroutine grad_omp(f,df1,df2,df_amp,df_max)
    implicit none
    real(kind=8), dimension(:,:), intent(in) :: f
    real(kind=8), dimension(size(f,1),size(f,2)), intent(out) :: df1,df2,df_amp
    real(kind=8), intent(out) :: df_max
    integer :: i, j, thread_id

    N = n1
    dx = dx1

    !$OMP parallel do
    do i = 1, n1
        call cfd4(f(i,:), df2(i,:))
    end do
    !$OMP end parallel do
    N = n2
    dx = dx2

    !$OMP parallel do
    do j = 1, n2
        call cfd4(f(:,j), df1(:,j))
    end do
    !$OMP end parallel do

    df_amp = abs(df2) + abs(df1)

    df_max = maxval(df_amp)

end subroutine grad_omp
!---------------------------
subroutine test_grad_omp(error,time)
    !tests accuracy and speed of grad_omp, assumes module variables n1,n2,dx1,dx2,numthreads have been set in calling program
    implicit none
    integer :: i1,j1
    real(kind=8), allocatable, dimension(:) :: x1,x2 !coordinates stored in arrays
    real(kind=8), allocatable, dimension(:,:) :: x11,x22 !coordinates stored in matrices
    real(kind=8), allocatable, dimension(:,:) :: ftest,df1,df2,df_amp,df_amp_exact !test function and results frorom grad
    real(kind=8) :: df_max
    integer(kind=8) :: t1,t2,clock_rate
    real(kind=8), intent(out) :: time,error(2) !error is two-element array
    integer :: time_counter1, time_counter2

    allocate(x1(n1),x2(n2),x11(n2,n1),x22(n2,n1),ftest(n2,n1),df1(n2,n1),df2(n2,n1), &
        df_amp(n2,n1),df_amp_exact(n2,n1))

    !generate mesh
    dx1 = 1.d0/dble(n1-1)
    dx2 = 1.d0/dble(n2-1)

    do i1=1,n1
        x1(i1) = dble(i1-1)*dx1
    end do

    do i1=1,n2
        x2(i1) = dble(i1-1)*dx2
    end do

    do i1=1,n2
        x11(i1,:) = x1
    end do

    do i1=1,n1
        x22(:,i1) = x2
    end do

    ftest = sin(x11)*cos(x22) !test function

    call omp_set_num_threads(numthreads)

    ! Perform grad calculation, recording wall time
    call system_clock(time_counter1)
    call grad_omp(ftest,df1,df2,df_amp,df_max)
    call system_clock(time_counter2)
    time = time_counter2 - time_counter1

    ! Calculate error in grad compared to analytical value
    error(1) = sum(abs(cos(x11)*cos(x22) - df1))
    error(2) = sum(abs(-sin(x11)*sin(x22) - df2))


end subroutine test_grad_omp
!---------------------------
end module fdmodule2d














program test
    use fdmodule2d
    implicit none
    real(kind=8) :: time, error(2)
!    real(kind=8), dimension(5, 5) :: f, df1, df2, df_amp
!    real(kind=8) :: df_max
!    integer :: i, j
!    N = 5
!    n1 = N
!    n2 = N
!    dx = 1
!    dx1 = dx
!    dx2 = dx
!    do i = 1,N
!        do j = 1, N
!            f(i, j) = i*j*dx
!        end do
!    end do
!    call grad(f,df1,df2,df_amp,df_max)
!    print *, df_max
    n1 = 1000
    n2 = 1000
    numthreads = 4
    call test_grad(error, time)
    print *, "Non-parallel:", time, error
    call test_grad_omp(error, time)
    print *, "Parallel:", time, error
end program test

"""Name
8-digit college id
"""

#import modules here
import numpy as np
import matplotlib.pyplot as plt
import numpy.random as npr
import math

#---------------------
def test_randn(n,m=0,s=1):
    """Input variables:
    n   number of normal random variables to be generated
    m   random variables generated with mean = m
    s   random variables generated with standard deviation = s
    """
    rand_numbers = [m + npr.randn()*s for i in range(n)]
    plt.hist(rand_numbers)
    return rand_numbers
        
 
#---------------------
def wiener1(dt,X0,Nt):
    """ Input variables:
    dt    time step
    X0    intial value, X(t=0) = X0
    Nt    number of time steps 
    """
    X = np.arange(Nt + 1, dtype=np.float)
    X[0] = X0
    for i in range(1, Nt + 1):
        X[i] = X[i-1] + math.sqrt(dt) * npr.randn()
    plt.plot(X)
    plt.show()
    
    
#---------------------
def wienerM(dt,X0,Nt,M):
    """ Input variables:
    dt    time step
    X0    intial value, X(t=0) = X0
    Nt    number of time steps 
    M     number of samples
    """
    X = np.empty([Nt + 1, M], dtype=np.float)
    X[0,:] = X0
    for i in range(1, Nt + 1):
        for j in range(M):
            X[i, j] = X[i-1, j] + math.sqrt(dt) * npr.randn()
    mean = np.mean(X, axis=1)
    std = np.std(X, axis=1)
    actual_mean = np.zeros(Nt)
    actual_std = np.sqrt(dt*np.arange(Nt+1))
    plt.plot(mean)
    plt.plot(actual_mean)
    plt.plot(std)
    plt.plot(actual_std)
    plt.show()

#-----------------------------
def langevin1(dt,V0,Nt,g=1.0):
    """ Input variables:
    dt    time step
    V0    intial value, V(t=0) = V0
    Nt    number of time steps 
    g     friction factor, gamma
    """
    V = np.arange(Nt + 1, dtype=np.float)
    V[0] = V0
    for i in range(1, Nt + 1):
        V[i] = V[i-1] + math.sqrt(dt) * npr.randn() - g*V[i-1]*dt
    plt.plot(V)
    plt.show()
    

#-----------------------------
def langevinM(dt,V0,Nt,M,g=1.0,debug=False):
    """ Input variables:
    dt    time step
    V0    intial value, V(t=0) = V0
    Nt    number of time steps 
    M     number of samples
    g     friction factor, gamma
    debug flag to control plotting of figures
    """
    V = np.empty([Nt + 1, M], dtype=np.float)
    V[0,:] = V0
    for i in range(1, Nt + 1):
        for j in range(M):
            V[i, j] = V[i-1, j] + math.sqrt(dt) * npr.randn() - g*V[i-1, j]*dt
    mean = np.mean(V, axis=1)
    std = np.std(V, axis=1)
    actual_mean = V0 * np.exp(-g*dt*np.arange(Nt+1, dtype=np.float))
    actual_std = np.sqrt((1-np.exp(-2*g*dt*np.arange(Nt+1))) / (2*g))
    if (debug == True):
		plt.plot(mean)
		plt.plot(actual_mean)
		plt.plot(std)
		plt.plot(actual_std)
		plt.show()
    epsilon = 0
    for i in range(Nt + 1):
        epsilon += (1.0/Nt) * abs(mean[i] - actual_mean[i])
    return epsilon

#--------------------------------
def langevinTest(dt,V0,Nt,g=1.0):
    """ Input variables:
    dt    time step
    V0    intial value, V(t=0) = V0
    Nt    number of time steps 
    g     friction factor, gamma
    """
    epsilon_vals = []
    M_vals = [10, 100, 1000, 10000]
    for M in M_vals:
        epsilon_vals.append(langevinM(dt, V0, Nt, M, g, False))
    fit = np.polyfit(np.log(M_vals), np.log(epsilon_vals), 1)
    plt.loglog(M_vals, epsilon_vals, 'o')
    plt.loglog(M_vals, np.power(M_vals, fit[0]) * math.exp(fit[1]))
    plt.show()
    exponent = fit[0]
    print exponent
    return exponent
#---------------------------------
#The section below is included for assessment and *must* be included exactly as below in the final
#file that you submit, but you may want to omit it (e.g. comment it out) while you are developing
#your code.  
"""if __name__ == "__main__":
    npr.seed(1)
    test_randn(1000)
    wiener1(0.1,0.0,1000)
    wienerM(0.1,2.0,1000,20000)
    langevin1(0.1,0.0,1000,0.01)
    print "Error from LangevinM, dt=%1.2f,V0=%1.2f,Nt=%d,M=%d,g=%1.3f: "%(0.1,1.0,1000,2000,0.05), langevinM(0.1,1.0,1000,2000,0.05,True)
    print "Convergence rate for Langevin Monte Carlo calculations from langevinTest", langevinTest(0.1,1.0,1000,0.25)
    plt.show()"""

if __name__ == "__main__":
    langevinTest(0.01, 1, 1000)
